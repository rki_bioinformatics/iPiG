# iPiG

--- **i**ntegrating **P**eptide Spectrum Matches **i**nto **G**enome Browser Visualizations ---

Please find the iPiG repository at [iPiG](https://gitlab.com/mkuhring/iPiG).

iPiG targets the integration of peptide spectrum matches (PSMs) from mass spectrometry (MS) peptide identifications into genomic visualisations provided by genome browser such as the [UCSC genome browser](http://genome.ucsc.edu/).

iPiG takes PSMs from the MS standard format mzIdentML (*.mzid) or in text format and provides results in genome track formats (BED and GFF3 files), which can be easily imported into genome browsers.

For more details about iPiG and it's functionallity, please see:  
[iPiG: Integrating Peptide Spectrum Matches Into Genome Browser Visualizations](http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0050246)
Mathias Kuhring and Bernhard Y. Renard, 2012, PLOS ONE
